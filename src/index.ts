import { connect, Document, Schema, Model, model } from 'mongoose';
import { Pessoa } from './interface/Pessoa';
import { PessoaSchema } from './model/PessoaSchema';

export interface PessoaDocument extends Pessoa, Document { }

const PessoaModel = model<PessoaDocument>('Pessoa', PessoaSchema, 'pessoas');

async function main() {
    try {
        const url: string = 'mongodb+srv://db:db@node.uio0p.mongodb.net/db?retryWrites=true&w=majority';
        const client = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('connected');
        const doc = await PessoaModel.create({ nome: 'Ncjhsnnn', idade: 120 }); //CREATING PERSON
        // await PessoaModel.deleteOne({nome: 'John Doe'}).exec(); // DELETE FINDING BY NAME
        const pessoa = await PessoaModel.find().where('nome').equals('Ncjhsnnn');
         console.log(pessoa);

        (await PessoaModel.find().lean().exec()).forEach(p => {
            console.log(p.nome); // SHOW PEOPLE NAME
        });

        if (client && client.connection) {
            await client.connection.close();
        }

    } catch (err) {
        console.log('deu bret');
        console.log(err.message);
    }
};


main();