import { connect, model, Schema } from 'mongoose';

export const pessoaSchema = new Schema({
    nome: {
        type: String,
        required: true
    },
    idade: {
        type: Number,
        required: true,
        min: 0
    }
});

async function main() {
    try {
        const url = 'mongodb://localhost:27017/nty';
        const client = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('connected');
        const pessoaModel = model('Pessoa', pessoaSchema, 'pessoas');
        const pessoaDocument = new pessoaModel({ nome: 'John', idade: 20 });
        // await pessoaDocument.save(); -- insere
        // const pessoas = await pessoaModel.find().exec(); -- busca todas as pessoas
        const pessoa = await pessoaModel.findById('5f0df477cd182d3158530675').exec(); //-- busca uma pessoa pelo campo
        // await pessoa?.set('idade', 50); // editar
        // await pessoa?.save();
        // await pessoa?.remove(); //remover
        

        if (client && client.connection) {
            await client.connection.close();
        }
    } catch (err) {
        console.log('deu bret');
        console.log(err.message);
    }
}

main();