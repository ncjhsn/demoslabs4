import { Schema } from 'mongoose';
import { Pessoa } from '../interface/Pessoa';

export interface PessoaDocument extends Pessoa, Document { }

export const PessoaSchema = new Schema({
    nome: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 50
    },
    idade: {
        type: Number,
        required: true,
        min: 0
    }
});
