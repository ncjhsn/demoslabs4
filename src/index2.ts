import { MongoClient } from 'mongodb';

async function main() {
    const url = 'mongodb+srv://db:db@node.uio0p.mongodb.net/nty?retryWrites=true&w=majority';
    try {
        const cliente = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('connected');
        const db = cliente.db('nty');
        const collection = db.collection('teste');
        const data = collection.find({});

        await collection.findOne({nome: 'edgar'});
        console.log('=-=-=-=-=-=');
        await data.forEach((doc) => {
            console.log(doc.nome);
        })

        await collection.deleteOne({ nome: 'idk' });
        console.log('deleted, nao deu bret');

        await data.forEach((doc) => {
            console.log(doc.nome);
        })

        cliente.close();
    } catch (err) {
        console.log('Deu bret');
        console.log(err.message);

    }
}

main();